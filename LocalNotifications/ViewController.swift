//
//  ViewController.swift
//  LocalNotifications
//
//  Created by alexfb on 7/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController,UNUserNotificationCenterDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        UNUserNotificationCenter.current().delegate=self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            
        }
         let repeatAction=UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        let category=UNNotificationCategory(identifier: "myCategory", actions: [repeatAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }

    var badge=0
    func sendNotification(){
        //Contenido
badge+=1
        let content=UNMutableNotificationContent()
        content.title="Alarma"
        content.subtitle="Mi nueva alarma"
        content.body="Despiérate!!!"
        content.categoryIdentifier="myCategory"
        content.badge=badge as NSNumber
        
        
        //Trigger
        let trigger=UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        //Request
        let request=UNNotificationRequest(identifier: "alarma", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request){(error)in
        
        }
    }
func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        switch response.actionIdentifier{
        case "repeat":
            sendNotification()
        default:
            print("Other Action")
        }
        completionHandler()
    }
    @IBAction func activarNotificacion(_ sender: Any) {
        sendNotification()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

